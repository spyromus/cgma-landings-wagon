# README

Site configurations repository. Subdirectories contain per-site configuration.

## Dockerization

### General

This repo is dockerized to isolate dependencies (Ruby version, gems etc).

Running container:

```shell
$ docker-compose run --rm -it wagon
```

### Configuration

Each school has separate Wagon tool installation that needs gems installed.

Use `bin/bundle-install <site>` to install gems for the site.

```shell
$ bin/bundle-install main
$ bin/bundle-install school
```

### Running wagon command

There are helper scripts to run wagon command inside the container:

```shell
$ bin/wagon-main --help
$ bin/wagon-school --help
```

### Serving

```
$ bin/wagon-main serve -h 0.0.0.0
$ bin/wagon-school serve -h 0.0.0.0
```

Then open: http://localhost:3333
