# Human readable name of this type
name: Master courses

# Lowercase, underscored handle used to access this type
slug: master_courses

# Explanatory text displayed in the back-office
description: A description of the content type for the editors

# Slug of field used to identify entries by default, such as the title
label_field_name: title

# Valid values: manually, created_at, updated_at, or the slug of any field
order_by: title

# Valid values: asc (ascending) and desc (descending). Set to asc by default.
# order_direction: asc

# Specify a field slug to group entries by that field in the back-office.
# group_by: <your field>

# Activate public 'create' API (e.g for a contact form)
# public_submission_enabled: false

# Array of emails to be notified of new entries made with the public API
# public_submission_accounts: ['john@example.com']

# Add the files attached to the form to the notification email
# public_submission_email_attachments: false

# Set the title of the notification email
# public_submission_title_template: "{{ entry.name }} wants to reach you"

# we recommend to require Google Recaptcha when submitting the form
# if the public submission attribute is enabled in order to prevent spams
# recaptcha_required: true

# Control the display of the content type in the back-office.
# display_settings:
#   seo: false              # display the SEO tab for the content entries
#   advanced: false         # display the Advanced tab for the content entries
#   position: 1             # position in the sidebar menu
#   hidden: false           # hidden for authors?

# By default, the back-office displays the _label property (see label_field_name) of the content entry. This can be modified by writing your own Liquid template below:
# entry_template: '<a href="{{ link }}">{{ entry._label }}</a>' # The default template

# A list describing each field
fields:
- unity_id:
    label: Unity ID
    type: integer
    required: true
    hint: Unity ID
    localized: false

- title:
    label: Title
    type: string
    required: true
    hint: Course title
    localized: false

- description:
    label: Description
    type: text
    required: false
    hint: Course description
    localized: false
    text_formatting: html

- slug:
    label: Slug
    type: string
    required: true
    hint: URL slug
    localized: false

- courses:
    label: Courses
    type: has_many
    required: false
    hint: Associated courses
    localized: false
    class_name: courses
    inverse_of: master_course
    ui_enabled: true

- schools:
    label: Schools
    type: many_to_many
    required: false
    hint: Schools teaching this course
    localized: false
    class_name: schools
    inverse_of: master_courses
    ui_enabled: true

- default_school:
    label: Default school
    type: belongs_to
    required: false
    hint: Default school to navigate when clicking on the course
    localized: false
    class_name: schools
    inverse_of:
    ui_enabled: true

- on_home:
    label: On home
    type: boolean
    required: false
    hint: Show this course on home page
    localized: false

- static_content:
    label: Static content
    type: text
    required: false
    hint: Some static content
    localized: false
    text_formatting: html

- image:
    label: Image
    type: file
    required: false
    hint: Course image
    localized: false
